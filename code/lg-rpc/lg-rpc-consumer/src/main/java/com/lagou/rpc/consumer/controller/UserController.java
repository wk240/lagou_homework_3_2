package com.lagou.rpc.consumer.controller;

import com.lagou.rpc.api.IUserService;
import com.lagou.rpc.consumer.proxy.RpcClientProxy;
import com.lagou.rpc.pojo.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author panwenkang
 * @date 2021/5/7
 */
@RestController
public class UserController {

    @GetMapping("/user/{userId}")
    public User getById(@PathVariable Integer userId) {
        //获取远程服务的代理类
        IUserService userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
        User user = userService.getById(userId);
        return user;
    }
}
