package com.lagou.rpc.consumer.client;

import com.lagou.rpc.consumer.handler.RpcClientHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 客户端
 * 1.连接Netty服务端
 * 2.提供给调用者主动关闭资源的方法
 * 3.提供消息发送的方法
 */
public class RpcClient {

    private EventLoopGroup group;

    private Channel channel;

    private String ip;

    private int port;

    //服务端响应时间 单位毫秒
    private long responseTimeMs;
    //最近一次请求时间 单位毫秒
    private long latestRequestTimeMs;

    private RpcClientHandler rpcClientHandler = new RpcClientHandler();

    private ExecutorService executorService = Executors.newCachedThreadPool();

    public RpcClient(String serverAddress){
        String[] server = serverAddress.split(":");
        this.ip = server[0];
        this.port = Integer.parseInt(server[1]);
        initClient();
    }

    /**
     * 初始化方法-连接Netty服务端
     */
    public void initClient() {
        try {
            //1.创建线程组
            group = new NioEventLoopGroup();
            //2.创建启动助手
            Bootstrap bootstrap = new Bootstrap();
            //3.设置参数
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, Boolean.TRUE)
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 3000)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            ChannelPipeline pipeline = channel.pipeline();
                            //String类型编解码器
                            pipeline.addLast(new StringDecoder());
                            pipeline.addLast(new StringEncoder());
                            //添加客户端处理类
                            pipeline.addLast(rpcClientHandler);
                        }
                    });
            //4.连接Netty服务端
            channel = bootstrap.connect(ip, port).sync().channel();
        } catch (Exception exception) {
            exception.printStackTrace();
            if (channel != null) {
                channel.close();
            }
            if (group != null) {
                group.shutdownGracefully();
            }
        }
    }

    /**
     * 提供给调用者主动关闭资源的方法
     */
    public void close() {
        if (channel != null) {
            channel.close();
        }
        if (group != null) {
            group.shutdownGracefully();
        }
    }

    /**
     * 提供消息发送的方法
     */
    public Object send(String msg) throws ExecutionException, InterruptedException {
        rpcClientHandler.setRequestMsg(msg);

        //记录响应时间
        //请求开始时间
        long startTimestamp = System.currentTimeMillis();

        //向服务端发送请求
        Future submit = executorService.submit(rpcClientHandler);
        //获取响应结果
        Object res = submit.get();
        Thread.sleep(5);

        //请求结束时间
        long endTimestamp = System.currentTimeMillis();
        //记录服务端响应时间 单位毫秒
        responseTimeMs = endTimestamp - startTimestamp;
        //记录当前最新的请求时间
        latestRequestTimeMs = System.currentTimeMillis();
        System.out.println(ip + ":" + port + " 响应时间更新为 " + responseTimeMs + "ms");
        System.out.println();
        System.out.println();

        return res;
    }

    @Override
    public String toString() {
        return ip + ":" + port;
    }

    public long getResponseTimeMs() {
        return responseTimeMs;
    }

    public void setResponseTimeMs(long responseTimeMs) {
        this.responseTimeMs = responseTimeMs;
    }

    public long getLatestRequestTimeMs() {
        return latestRequestTimeMs;
    }

    public void setLatestRequestTimeMs(long latestRequestTimeMs) {
        this.latestRequestTimeMs = latestRequestTimeMs;
    }
}
