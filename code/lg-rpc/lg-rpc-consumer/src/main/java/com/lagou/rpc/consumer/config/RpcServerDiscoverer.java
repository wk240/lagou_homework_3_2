package com.lagou.rpc.consumer.config;

import com.lagou.rpc.consumer.client.RpcClient;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.*;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Rpc服务端发现
 */
public class RpcServerDiscoverer {

    /**
     * 在线服务端集合
     */
    private static List<RpcClient> clients = new CopyOnWriteArrayList<>();

    /**
     * 服务连接下标，负载均衡实现
     */
    private static AtomicInteger atomicIndex = new AtomicInteger();

    public static void init() {
        //连接zookeeper
        CuratorFramework zkClient = CuratorFrameworkFactory.builder()
                .connectString("120.79.87.211:2181,120.79.87.211:2182,120.79.87.211:2183")
                .sessionTimeoutMs(5000)
                .connectionTimeoutMs(5000)
                .retryPolicy(new ExponentialBackoffRetry(1000, 3))
                .build();
        zkClient.start();

        //获取在线的服务端地址，缓存到本地的服务端集合中。
        try {
            List<String> serverAddressList = zkClient.getChildren().forPath("/clusterserver");
            for (String serverAddress : serverAddressList) {
                //创建连接并加入在线服务端集合中
                clients.add(new RpcClient(serverAddress));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //监听服务端节点增加、删除事件
        PathChildrenCache pathChildrenCache = new PathChildrenCache(zkClient, "/clusterserver", true);
        //添加监听事件
        pathChildrenCache.getListenable().addListener(new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                if (pathChildrenCacheEvent.getData() == null) {
                    return;
                }
                //获取变动的节点路径
                String path = pathChildrenCacheEvent.getData().getPath();
                switch (pathChildrenCacheEvent.getType()){
                    //子节点添加事件
                    case CHILD_ADDED:
                        String onlineServerAddress = path.split("/")[2];
                        //创建连接并加入在线服务端集合中
                        clients.add(new RpcClient(onlineServerAddress));
                        System.out.println("建立连接 "+onlineServerAddress);
                        break;
                    //子节点删除事件
                    case CHILD_REMOVED:
                        String offlineServerAddress = path.split("/")[2];
                        for (RpcClient client : clients) {
                            if(client.toString().equals(offlineServerAddress)) {
                                //从在线服务端集合中删除
                                clients.remove(client);
                                //关闭连接
                                client.close();
                                break;
                            }
                        }
                        System.out.println("断开连接 "+offlineServerAddress);
                        break;
                    //节点数据变更时间
                    case CHILD_UPDATED:
                        if(pathChildrenCacheEvent.getData().getData() != null) {
                            System.out.println(path + " -> " + new String(pathChildrenCacheEvent.getData().getData()));
                        }
                        break;
                }
            }
        });
        try {
            pathChildrenCache.start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        job(zkClient);
    }

    /**
     * 循环模拟实现定时任务，定时上报响应时间
     *
     * @param zkClient
     */
    public static void job(CuratorFramework zkClient) {
        new Thread(()->{
            while (true) {
                try {
                    //睡眠5秒
                    TimeUnit.SECONDS.sleep(5);
                    for (RpcClient client : clients) {
                        //最后一次请求时间距离当前时间超过5秒，响应时间清零
                        if (System.currentTimeMillis() - client.getLatestRequestTimeMs() >= 5000) {
                            client.setResponseTimeMs(0);
                        }

                        //修改zookeeper节点数据，更新响应时间
                        zkClient.setData().forPath("/clusterserver/" + client, (client.getResponseTimeMs()+"").getBytes());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * 获取连接
     *
     * @return
     */
    public static RpcClient getClient() {
        //1、最后一次请求时间距离当前时间超过5秒，响应时间清零
        for (RpcClient client : clients) {
            if (System.currentTimeMillis() - client.getLatestRequestTimeMs() >= 5000) {
                client.setResponseTimeMs(0);
            }
        }

        //2、随机获取一个服务端
        int randomIndex = new Random().nextInt(clients.size());
        RpcClient target = clients.get(randomIndex);

        //3、尝试获取响应时间最小的服务端
        long minMs = target.getResponseTimeMs();
        for (int i = 0; i < clients.size(); i++) {
            //是否有比随机服务端响应时间更小的，没有的话那么就都是大于或等于的了
            if(clients.get(i).getResponseTimeMs() < minMs) {
                minMs = clients.get(i).getResponseTimeMs();
                target = clients.get(i);
            }
            System.out.println(clients.get(i).toString() + " -> " + clients.get(i).getResponseTimeMs() + "ms");
        }
        System.out.println("访问服务器 ----> " + target.toString());

        return target;
    }

    public static void close(){
        for (RpcClient client : clients) {
            client.close();
        }
    }
}
