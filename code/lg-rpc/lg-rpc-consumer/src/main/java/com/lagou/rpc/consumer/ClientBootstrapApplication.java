package com.lagou.rpc.consumer;

import com.lagou.rpc.consumer.config.RpcServerDiscoverer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 客户端启动类
 *
 * @author panwenkang
 * @date 2021/5/7
 */
@SpringBootApplication
public class ClientBootstrapApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ClientBootstrapApplication.class);
    }

    @Override
    public void run(String... args) throws Exception {
        //rpc服务端发现者类初始化
        RpcServerDiscoverer.init();
    }
}
