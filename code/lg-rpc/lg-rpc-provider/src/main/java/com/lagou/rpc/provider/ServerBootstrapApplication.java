package com.lagou.rpc.provider;

import com.lagou.rpc.provider.server.RpcServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 服务端启动类
 */
@SpringBootApplication
public class ServerBootstrapApplication implements CommandLineRunner {


    @Autowired
    RpcServer rpcServer;

    public static void main(String[] args) {
        SpringApplication.run(ServerBootstrapApplication.class, args);
    }

    @Override
    public void run(final String... args) throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String ip = "127.0.0.1";
                int port = 8899;
                if (args != null && args.length == 2) {
                    ip = args[0];
                    port = Integer.parseInt(args[1]);
                }
                rpcServer.startServer(ip, port);
            }
        }).start();
    }
}
